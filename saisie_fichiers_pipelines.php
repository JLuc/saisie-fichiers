<?php

function saisie_fichiers_formulaire_verifier($flux) {
	
	if ($flux['args']['form'] == 'formidable' and count($_FILES)) {
		
		// retrouver et lister les saisies
		$id_formulaire = intval(_request('id_formulaire'));
		$formulaire = sql_fetsel('*', 'spip_formulaires', 'id_formulaire = '.$id_formulaire);
		$saisies = unserialize($formulaire['saisies']);
		#$saisies = saisies_lister_par_type($saisies);
		$saisies = saisies_lister_par_nom($saisies);
		
		// pour chaque fichier poste, on ne prend que ce qui a été transmis 
		// et qui est bien demandé par ce formulaire.
		foreach ($_FILES as $name => $desc) {
			if ($desc['error'] != 4) { // un fichier est effectivement envoye 
				if (isset($saisies[$name]) and substr($saisies[$name]['saisie'],0,5) == 'file_') { // la saisie existe pour ce name
					if (!$desc['error']) {
						include_spip('inc/saisies_fichiers');
						$ok_ou_erreur = saisir_fichier($saisies[$name], $desc, array(
							'id_formulaire' => $id_formulaire,
							//!\ risque de poser problème si l'on souhaite modifier 
							// la réponse de quelqu'un d'autre que nous...
							'id_auteur' => $GLOBALS['visiteur_session']['id_auteur'],
						));
						
						if (is_string($ok_ou_erreur)) {
							$flux['data'][$name] = $ok_ou_erreur;
						}
					}
					
					// erreur sur l'upload
					else {
						$flux['data'][$name] = _T('saisie_fichier:erreur_upload_fichier');
					}					
				}
			}
		}
		/*
		// y a t'il un type file ?
		if (is_array($saisies['file_pdf'])) {
			$files = $saisies['file_pdf'];
			foreach ($files as $name => $desc) {
				if (isset($_FILES[$name])) {
					$fichier = $_FILES[$name];
					if (!$fichier['error']) {
						
						// on stocke dans /form/$id/$id_auteur_ + hash.pdf
						sous_repertoire($dir = _DIR_IMG . 'form/');
						sous_repertoire($dir .= "$id_formulaire/");
						$i = pathinfo($fichier['name']);
						$i['extension'] = strtolower($i['extension']);
						
						// n'autoriser que les pdf
						if ($i['extension'] == 'pdf') {
							
							$nom = $GLOBALS['visiteur_session']['id_auteur'] .  '_' 
								. substr(md5($name), 0, 6) . '.' . $i['extension'];
							
							// deplacer au nouvel emplacement.
							include_spip('inc/getdocument');
							if (deplacer_fichier_upload($fichier['tmp_name'], $dir . $nom, true)) {
								// donner a manger au traiter en forcant l'enregistrement
								// sur la localisation du fichier
								set_request($name, "form/$id_formulaire/" . $nom);
							}
						} else {
							$flux['data'][$name] = _T('saisie_fichier:erreur_format_pdf');
						}


					} else {
						// 4 : pas de fichier envoye...
						if ($fichier['error'] != 4) {
							$flux['data'][$name] = _T('saisie_fichier:erreur_upload_fichier');
						}
					}

				}
			}
		}*/
	}
	return $flux;
}

?>
